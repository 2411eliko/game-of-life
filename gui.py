from tkinter import (
    Label,
    RIDGE,
    Tk,
    NSEW,
    Entry,
    END,
    ttk,
    OptionMenu,
    StringVar
)

from game import GameOfLife


class Animation:
    def __init__(self, game: GameOfLife):
        self.game = game
        self.root = Tk()

    def play(self):
        next(self.game)

        for i, r in enumerate(self.game.board):
            for t, c in enumerate(r):
                lbl = Label(self.root,
                            text='*' if c else '',
                            relief=RIDGE,
                            padx=10,
                            pady=10
                            )
                lbl.grid(row=i, column=t, sticky=NSEW)

        self.root.after(self.game.delay, self.play)

    def start(self):
        self.root.after(self.game.delay, self.play)
        self.root.mainloop()


class GetData:
    def __init__(self):
        # default for size and level
        self.size = 10
        self.level = 2
        self.delay = 500

        self.win = Tk()
        self.delay_var = StringVar(self.win)
        self.size_field = Entry(self.win, width=40)
        self.level_field = Entry(self.win, width=40)

        self.start()

    def order_buttons(self):
        # size
        Label(self.win, text="Type here grid size").pack()
        self.size_field.insert(END, '10')
        self.size_field.focus_set()
        self.size_field.pack()

        # level of starting point
        Label(
            self.win,
            text="Type here the level of living cells (1, 2 or 3)\n"
                 " for the starting point"
        ).pack()

        self.level_field.insert(END, '2')
        self.level_field.pack()

        # delay
        Label(self.win, text="Choose delay between generations").pack()
        options = [str(x * 500) for x in range(1, 5)]  # 0.5 to 2 sec.

        self.delay_var.set(options[0])  # default value to 500 ms
        delay = OptionMenu(self.win, self.delay_var, *options)
        delay.pack()

        # ok button
        ttk.Button(
            self.win,
            text="OK",
            width=20,
            command=self.get_data
        ).pack(pady=20)

    def start(self):
        self.order_buttons()
        self.win.mainloop()

    def get_data(self):
        self.size = int(self.size_field.get())
        self.level = int(self.level_field.get())
        self.delay = int(self.delay_var.get())
        self.win.destroy()
