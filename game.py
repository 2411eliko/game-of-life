from copy import deepcopy
from time import sleep
from random import randint


class GameOfLife:
    def __init__(self, board=None, size=10, level=2, delay=500):
        if board:
            self.board = board
        else:
            self.board = self.create_board(size, level)
        self.delay = delay  # ms

    def play(self):
        for i in self:
            print(i)
            sleep(self.delay / 1000)  # ms to second

    @staticmethod
    def create_board(size, level):
        # initialize board with 0
        board: list[list] = [[0 for _ in range(size)] for _ in range(size)]
        num = 25 + 25 * level  # 50%, 75%, 100%

        for i in range(num*(size*size)//100):  # percentage * size / 100 
            board[randint(0, size-1)][randint(0, size-1)] = 1
        return board

    def __str__(self):
        return '\n'.join(
            ' '.join('*' if col else '.' for col in row)
            for row in self.board
        ) + '\n'

    def __iter__(self):
        return self

    def __next__(self):
        new_board = deepcopy(self.board)

        # todo: add ability to use different shapes
        size = len(self.board)

        for r in range(size):
            for c in range(size):
                # initialize counter for live neighbours.
                live_neighbours = 0

                optional_locations = (  # 8 positions
                    (r-1, c-1), (r-1, c), (r-1, c+1),
                    (r, c-1), (r, c+1),
                    (r+1, c-1), (r+1, c), (r+1, c+1)
                )

                for x, y in optional_locations:
                    if all((x >= 0 <= y, x < size, y < size)):
                        live_neighbours += self.board[x][y]

                if self.board[r][c]:  # alive
                    # 'int' is not necessary, but it is more esthetically.
                    new_board[r][c] = int(live_neighbours in (2, 3))
                else:
                    new_board[r][c] = int(live_neighbours == 3)

        self.board = new_board
        return self


if __name__ == '__main__':
    a = GameOfLife()
    a.play()


# grid = [
#     [0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
#     [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
#     [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
#     [1, 0, 1, 1, 1, 0, 0, 0, 0, 0],
#     [0, 0, 1, 1, 0, 0, 1, 0, 1, 0],
#     [1, 1, 1, 0, 0, 0, 1, 0, 0, 1],
#     [1, 0, 1, 0, 1, 1, 0, 0, 1, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
#     [1, 0, 1, 1, 0, 1, 0, 1, 0, 0],
#     [0, 0, 1, 1, 0, 0, 0, 0, 1, 0]
#     ]
