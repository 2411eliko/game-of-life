from game import GameOfLife
from gui import Animation, GetData

data = GetData()

Animation(
    GameOfLife(
        # board optional, list of list with 0/1 values
        size=data.size,
        level=data.level,
        delay=data.delay
    )
).start()
